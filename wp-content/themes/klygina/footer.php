<!-- Bootstrap core JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Contact form JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jqBootstrapValidation.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="<?php echo get_template_directory_uri(); ?>/js/agency.min.js"></script>

</body>

</html>
