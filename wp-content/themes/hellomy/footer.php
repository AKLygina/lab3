	<!-- jQuery -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.min.js"></script>
	<!-- Count Down -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

	</body>
</html>
